package com.esoon.thread.threadImplementation.extendThread;

public class LambdaTest {
    public static void main(String[] args) {
        //匿名变量方式
        new Thread(() -> {
            int count = 0;
            for (int i = 0; i < 100; i++) {
                count += i;
            }
            System.out.println("count===" + count);
        }).start();
    }
}
