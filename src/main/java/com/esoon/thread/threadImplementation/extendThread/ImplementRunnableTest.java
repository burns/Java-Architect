package com.esoon.thread.threadImplementation.extendThread;

public class ImplementRunnableTest {
    public static void main(String[] args) {
        MyImplementRunnableClass myImplementRunnableClass = new MyImplementRunnableClass();
        Thread MyImplementRunnableClassThread = new Thread(myImplementRunnableClass);
        MyImplementRunnableClassThread.start();
    }
}

class MyImplementRunnableClass implements Runnable{
    @Override
    public void run() {
        System.out.println("burns MyImplementRunnableClass");
    }
}
