package com.esoon.thread.threadImplementation.extendThread;

public class ExtendThreadTest {
    public static void main(String[] args) {
        ExtendThread extendThread = new ExtendThread();
        extendThread.start();
    }
}

class ExtendThread extends Thread {
    @Override
    public void run() {
        System.out.println("burns extendThread");
    }
}
