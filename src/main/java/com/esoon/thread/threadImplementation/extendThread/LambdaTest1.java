package com.esoon.thread.threadImplementation.extendThread;

public class LambdaTest1 {
    public static void main(String[] args) {
        //非匿名变量方式
        Thread t = new Thread(() -> {
            int count = 0;
            for (int i = 0; i < 100; i++) {
                count += i;
            }
            System.out.println("count===" + count);
        });
//        变量启动线程
        t.start();
    }
}
